package day_01

import (
	"os"
	"strconv"
	"strings"
)

// reads the file passed as input and retuns an int slice
func read_input(file string) (res []int) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if n, err := strconv.Atoi(s); err == nil {
			res = append(res, n)
		}
	}
	return
}

func question1(file string) (res int) {
	input := read_input(file)
	for i := range input {
		if i > 0 && input[i] > input[i-1] {
			res++
		}
	}
	return
}

func question2(file string) (res int) {
	input := read_input(file)
	for i := range input {
		if i > 2 && input[i] > input[i-3] {
			res++
		}
	}
	return
}
