package day_02

import (
	"os"
	"strconv"
	"strings"
)

type Direction int

type Move struct {
	direction Direction
	value     int
}

const (
	up Direction = iota
	down
	forward
)

func parseMove(input string) Move {
	dir, num, found := strings.Cut(input, " ")
	if !found {
		panic("Input line format incorrect: " + input)
	}
	n, err := strconv.Atoi(num)
	if err != nil {
		panic(err)
	}
	switch dir {
	case "forward":
		return Move{forward, n}
	case "down":
		return Move{down, n}
	case "up":
		return Move{up, n}
	default:
		panic("Incorrect direction in input: " + dir)
	}
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (res []Move) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if s != "" {
			res = append(res, parseMove(s))
		}
	}
	return
}

func question1(file string) int {
	moves := read_input(file)
	depth, pos := 0, 0
	for _, m := range moves {
		switch m.direction {
		case up:
			depth -= m.value
		case down:
			depth += m.value
		case forward:
			pos += m.value
		}
	}
	return depth * pos
}

func question2(file string) (res int) {
	moves := read_input(file)
	depth, pos, aim := 0, 0, 0
	for _, m := range moves {
		switch m.direction {
		case up:
			aim -= m.value
		case down:
			aim += m.value
		case forward:
			pos += m.value
			depth += aim * m.value
		}
	}
	return depth * pos
}
