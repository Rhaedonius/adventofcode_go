package day_02

import "testing"

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 150
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 1855814
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 900
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 1845455714
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
