package day_03

import (
	"os"
	"strings"
)

type DiagnosticReport [][]int

func parseNumber(input string) (res []int) {
	for _, c := range input {
		res = append(res, int(c-48))
	}
	return
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (res DiagnosticReport) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if s != "" {
			res = append(res, parseNumber(s))
		}
	}
	return
}

func Gamma(report DiagnosticReport, bits int) (res int) {
	var counts []int
	for i := 0; i < bits; i++ {
		counts = append(counts, 0)
	}
	th := len(report) / 2
	for _, d := range report {
		for i, n := range d {
			counts[i] += n
		}
	}
	for _, c := range counts {
		n := 0
		if c > th {
			n = 1
		}
		res = (res << 1) + n
	}
	return
}

func Epsilon(gamma int, bits int) int {
	mask := 1<<bits - 1
	return ^gamma & mask
}

func oxygenGeneratorRating(report DiagnosticReport, pos int) int {
	if len(report) == 1 {
		res := 0
		for _, b := range report[0] {
			res = res<<1 + b
		}
		return res
	}
	var ones, zeros DiagnosticReport
	for _, r := range report {
		if r[pos] == 1 {
			ones = append(ones, r)
		} else {
			zeros = append(zeros, r)
		}
	}
	if len(zeros) > len(ones) {
		return oxygenGeneratorRating(zeros, pos+1)
	}
	return oxygenGeneratorRating(ones, pos+1)
}

func scrubberRating(report DiagnosticReport, pos int) int {
	if len(report) == 1 {
		res := 0
		for _, b := range report[0] {
			res = res<<1 + b
		}
		return res
	}
	var ones, zeros DiagnosticReport
	for _, r := range report {
		if r[pos] == 1 {
			ones = append(ones, r)
		} else {
			zeros = append(zeros, r)
		}
	}
	if len(ones) < len(zeros) {
		return scrubberRating(ones, pos+1)
	}
	return scrubberRating(zeros, pos+1)
}

func question1(file string) int {
	report := read_input(file)
	bits := len(report[0])
	gamma := Gamma(report, bits)
	epsilon := Epsilon(gamma, bits)
	return gamma * epsilon
}

func question2(file string) int {
	report := read_input(file)
	return oxygenGeneratorRating(report, 0) * scrubberRating(report, 0)
}
