package day_03

import "testing"

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 198
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 4147524
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 230
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 3570354
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
