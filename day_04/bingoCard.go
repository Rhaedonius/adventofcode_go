package day_04

import (
	"strconv"
	"strings"
)

type BingoCard struct {
	numbers [][]int
	marked  []int
}

// Parses an input string to create a BingoCard
func ParseBingoCard(input string) (res BingoCard) {
	for _, l := range strings.Split(input, "\n") {
		var row []int
		for _, num := range strings.Fields(l) {
			n, err := strconv.Atoi(num)
			if err != nil {
				panic(err)
			}
			row = append(row, n)
		}
		res.numbers = append(res.numbers, row)
	}
	res.marked = make([]int, 5)
	return
}

// Mark a number as drawn if exist on the card
func (b *BingoCard) MarkNumber(draw int) {
	for i, row := range b.numbers {
		for j, n := range row {
			if n == draw {
				b.marked[i] |= 1 << j
			}
		}
	}
}

// Check if the card is a winning one
func (b BingoCard) IsWinning() bool {
	mask := 1<<5 - 1
	res := mask
	for _, m := range b.marked {
		if m == mask {
			return true
		}
		res &= m
	}
	return res != 0
}

func (b BingoCard) FinalScore(draw int) int {
	unmarked := 0
	for i, m := range b.marked {
		for j := 0; j < 5; j++ {
			if m&(1<<j) == 0 {
				unmarked += b.numbers[i][j]
			}
		}
	}
	return draw * unmarked
}
