package day_04

import (
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

func parseLine(input string) int {
	if n, err := strconv.Atoi(input); err != nil {
		return n
	} else {
		panic(err)
	}
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (draws []int, cards []BingoCard) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	nums, cardtxt, found := strings.Cut(string(input), "\n\n")
	if !found {
		panic(fmt.Sprintf("Malformed input: %s", input))
	}
	for _, num := range strings.Split(nums, ",") {
		n, err := strconv.Atoi(num)
		if err != nil {
			panic(err)
		}
		draws = append(draws, n)
	}
	for _, card := range strings.Split(cardtxt, "\n\n") {
		if card != "" {
			c := ParseBingoCard(card)
			cards = append(cards, c)
		}
	}
	return
}

func question1(file string) int {
	draws, cards := read_input(file)
	for _, n := range draws {
		for _, c := range cards {
			c.MarkNumber(n)
			if c.IsWinning() {
				return c.FinalScore(n)
			}
		}
	}
	return 0
}

func question2(file string) int {
	draws, cards := read_input(file)
	var winning []int
	for _, n := range draws {
		for i, c := range cards {
			c.MarkNumber(n)
			if c.IsWinning() && !slices.Contains(winning, i) {
				winning = append(winning, i)
			}
			if len(winning) == len(cards) {
				return c.FinalScore(n)
			}
		}
	}
	return 0
}
