package day_04

import (
	"slices"
	"testing"
)

var testCard = BingoCard{
	numbers: [][]int{{14, 21, 17, 24, 4}, {10, 16, 15, 9, 19}, {18, 8, 23, 26, 20}, {22, 11, 13, 6, 5}, {2, 0, 12, 3, 7}},
	marked:  []int{31, 8, 4, 18, 19},
}

var testCard2 = BingoCard{
	numbers: [][]int{{14, 21, 17, 24, 4}, {10, 16, 15, 9, 19}, {18, 8, 23, 26, 20}, {22, 11, 13, 6, 5}, {2, 0, 12, 3, 7}},
	marked:  []int{12, 8, 10, 8, 9},
}

func TestWinning(t *testing.T) {
	if !testCard.IsWinning() {
		t.Error("testCard should be winning (horizontal match)")
	}
	if !testCard2.IsWinning() {
		t.Error("testCard2 should be winning. (vertical match)")
	}
}

func TestFinalScore(t *testing.T) {
	got := testCard.FinalScore(24)
	want := 4512
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestMarking(t *testing.T) {
	card := BingoCard{
		numbers: [][]int{
			{14, 21, 17, 24, 4},
			{10, 16, 15, 9, 19},
			{18, 8, 23, 26, 20},
			{22, 11, 13, 6, 5},
			{2, 0, 12, 3, 7},
		},
		marked: []int{0, 0, 0, 0, 0},
	}
	want := []int{31, 8, 4, 18, 19}
	draw := []int{7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24}
	for _, d := range draw {
		card.MarkNumber(d)
	}
	if !slices.Equal(card.marked, want) {
		t.Errorf("Result %v is not correct. (expected %v)", card.marked, want)
	}
}

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 4512
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 63424
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 1924
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 23541
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
