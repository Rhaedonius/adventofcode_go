package day_05

import (
	"os"
	"strings"
)

// reads the file passed as input and retuns an int slice
func read_input(file string) (res []Vent) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if s != "" {
			res = append(res, ParseVent(s))
		}
	}
	return
}

func countOverlaps(vents []Vent) (res int) {
	counts := make(map[Point]int)
	for _, v := range vents {
		for _, p := range v.GetPoints() {
			counts[p]++
		}
	}
	for _, v := range counts {
		if v > 1 {
			res++
		}
	}
	return
}

func question1(file string) int {
	input := read_input(file)
	var vents []Vent
	for _, v := range input {
		if v.start.x == v.end.x || v.start.y == v.end.y {
			vents = append(vents, v)
		}
	}
	return countOverlaps(vents)
}

func question2(file string) int {
	vents := read_input(file)
	return countOverlaps(vents)
}
