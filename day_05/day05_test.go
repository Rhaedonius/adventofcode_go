package day_05

import (
	"slices"
	"testing"
)

func TestGetPoints(t *testing.T) {
	t.Run("horizontal", func(t *testing.T) {
		v := Vent{Point{0, 4}, Point{2, 4}}
		got := v.GetPoints()
		want := []Point{{0, 4}, {1, 4}, {2, 4}}
		if !slices.Equal(got, want) {
			t.Errorf("Result %v is not correct. (expected %v)", got, want)
		}
	})
	t.Run("vertical", func(t *testing.T) {
		v := Vent{Point{0, 4}, Point{0, 1}}
		got := v.GetPoints()
		want := []Point{{0, 4}, {0, 3}, {0, 2}, {0, 1}}
		if !slices.Equal(got, want) {
			t.Errorf("Result %v is not correct. (expected %v)", got, want)
		}
	})
	t.Run("diagonal", func(t *testing.T) {
		v := Vent{Point{0, 4}, Point{2, 2}}
		got := v.GetPoints()
		want := []Point{{0, 4}, {1, 3}, {2, 2}}
		if !slices.Equal(got, want) {
			t.Errorf("Result %v is not correct. (expected %v)", got, want)
		}
	})
}

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 5
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 7436
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 12
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 21104
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
