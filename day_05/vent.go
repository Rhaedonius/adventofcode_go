package day_05

import (
	"strconv"
	"strings"
)

type Point struct {
	x int
	y int
}

type Vent struct {
	start Point
	end   Point
}

func parsePoint(input string) Point {
	before, after, found := strings.Cut(input, ",")
	if !found {
		panic("malformed input for Point (" + input + ")")
	}
	x, err := strconv.Atoi(before)
	if err != nil {
		panic(err)
	}
	y, err := strconv.Atoi(after)
	if err != nil {
		panic(err)
	}
	return Point{x, y}
}

func ParseVent(input string) Vent {
	before, after, found := strings.Cut(input, " -> ")
	if !found {
		panic("malformed input for Vent (" + input + ")")
	}
	start := parsePoint(before)
	end := parsePoint(after)
	return Vent{start, end}
}

func makeRange(start, end int) (res []int) {
	if start <= end {
		for i := start; i <= end; i++ {
			res = append(res, i)
		}
	} else {
		for i := start; i >= end; i-- {
			res = append(res, i)
		}
	}
	return
}

func (v Vent) GetPoints() (points []Point) {
	xrange := makeRange(v.start.x, v.end.x)
	yrange := makeRange(v.start.y, v.end.y)
	switch {
	case len(xrange) == 1:
		x := xrange[0]
		for _, y := range yrange {
			points = append(points, Point{x, y})
		}
		return
	case len(yrange) == 1:
		y := yrange[0]
		for _, x := range xrange {
			points = append(points, Point{x, y})
		}
		return
	case len(xrange) == len(yrange):
		for i := range xrange {
			points = append(points, Point{xrange[i], yrange[i]})
		}
		return
	default:
		panic("incorrect vent direction")
	}
}
