package day_06

import (
	"os"
	"strconv"
	"strings"
)

// reads the file passed as input and retuns an int slice
func read_input(file string) (res [9]int) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(strings.TrimSpace(string(input)), ",") {
		if s != "" {
			n, err := strconv.Atoi(s)
			if err != nil {
				panic(err)
			}
			res[n]++
		}
	}
	return
}

func countFish(fish [9]int, steps int) (res int) {
	for n := 0; n < steps; n++ {
		var next [9]int
		for i, f := range fish {
			if i == 0 {
				next[6] += f
				next[8] += f
			} else {
				next[i-1] += f
			}
		}
		fish = next
	}
	for _, f := range fish {
		res += f
	}
	return
}

func question1(file string) (res int) {
	fish := read_input(file)
	return countFish(fish, 80)
}

func question2(file string) (res int) {
	fish := read_input(file)
	return countFish(fish, 256)
}
