package day_06

import "testing"

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 5934
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 383160
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 26984457539
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 1721148811504
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
