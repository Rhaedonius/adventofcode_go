package day_07

import (
	"os"
	"strconv"
	"strings"
)

func parseLine(input string) int {
	if n, err := strconv.Atoi(input); err == nil {
		return n
	} else {
		panic(err)
	}
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (res []int) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(strings.TrimSpace(string(input)), ",") {
		if s != "" {
			res = append(res, parseLine(s))
		}
	}
	return
}

func find_min_max(arr []int) (int, int) {
	min, max := arr[0], arr[0]
	for _, a := range arr {
		if a < min {
			min = a
		}
		if a > max {
			max = a
		}
	}
	return min, max
}

func fuel_burn(arr []int, level int) (res int) {
	for _, a := range arr {
		d := a - level
		if d < 0 {
			res -= d
		} else {
			res += d
		}
	}
	return
}

func fuel_burn_incremental(arr []int, level int) (res int) {
	for _, a := range arr {
		d := a - level
		if d < 0 {
			d = -d
		}
		res += d * (d + 1) / 2
	}
	return
}

func question1(file string) int {
	input := read_input(file)
	min, max := find_min_max(input)
	res := 1<<32 - 1
	for l := min; l <= max; l++ {
		b := fuel_burn(input, l)
		if b < res {
			res = b
		}
	}
	return res
}

func question2(file string) int {
	input := read_input(file)
	min, max := find_min_max(input)
	res := 1<<32 - 1
	for l := min; l <= max; l++ {
		b := fuel_burn_incremental(input, l)
		if b < res {
			res = b
		}
	}
	return res
}
