package day_08

import (
	"os"
	"slices"
	"strings"
)

type Display struct {
	calibration []string
	value       []string
}

func parseDisplay(input string) (d Display) {
	before, after, found := strings.Cut(input, " | ")
	if !found {
		panic("Malformed input string :" + string(input))
	}
	for _, s := range strings.Split(before, " ") {
		if s != "" {
			d.calibration = append(d.calibration, s)
		}
	}
	for _, s := range strings.Split(after, " ") {
		if s != "" {
			d.value = append(d.value, s)
		}
	}
	return
}

func (d Display) countUnique() (res int) {
	for _, s := range d.value {
		switch len(s) {
		case 2:
			res++
		case 3:
			res++
		case 4:
			res++
		case 7:
			res++
		}
	}
	return
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (d []Display) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, line := range strings.Split(strings.TrimSpace(string(input)), "\n") {
		d = append(d, parseDisplay(line))
	}
	return
}

func (d Display) calibrate() (cmap map[rune]rune) {
	counts := make(map[rune]int)
	cmap = make(map[rune]rune)
	var s2, s3, s4, s7 string
	var found []rune
	for _, s := range d.calibration {
		switch len(s) {
		case 2:
			s2 = s
		case 3:
			s3 = s
		case 4:
			s4 = s
		case 7:
			s7 = s
		}
		for _, c := range s {
			counts[c]++
		}
	}
	for c, v := range counts {
		switch v {
		case 4:
			cmap['e'] = c
			found = append(found, c)
		case 6:
			cmap['b'] = c
			found = append(found, c)
		case 9:
			cmap['f'] = c
			found = append(found, c)
		}
	}
	for _, c := range s2 {
		if !slices.Contains(found, c) {
			cmap['c'] = c
			found = append(found, c)
			break
		}
	}
	for _, c := range s3 {
		if !slices.Contains(found, c) {
			cmap['a'] = c
			found = append(found, c)
			break
		}
	}
	for _, c := range s4 {
		if !slices.Contains(found, c) {
			cmap['d'] = c
			found = append(found, c)
			break
		}
	}
	for _, c := range s7 {
		if !slices.Contains(found, c) {
			cmap['g'] = c
			found = append(found, c)
			break
		}
	}
	return
}

func (d Display) decode(cmap map[rune]rune) (res int) {
	for _, s := range d.value {
		switch len(s) {
		case 2:
			res = res*10 + 1
		case 3:
			res = res*10 + 7
		case 4:
			res = res*10 + 4
		case 5:
			if strings.ContainsRune(s, cmap['e']) {
				res = res*10 + 2
			} else if strings.ContainsRune(s, cmap['c']) {
				res = res*10 + 3
			} else {
				res = res*10 + 5
			}
		case 7:
			res = res*10 + 8
		default:
			if !strings.ContainsRune(s, cmap['e']) {
				res = res*10 + 9
			} else if !strings.ContainsRune(s, cmap['c']) {
				res = res*10 + 6
			} else {
				res = res * 10
			}
		}
	}
	return
}

func question1(file string) (res int) {
	displays := read_input(file)
	for _, d := range displays {
		res += d.countUnique()
	}
	return
}

func question2(file string) (res int) {
	displays := read_input(file)
	out := make(chan int, 200)
	for _, d := range displays {
		go func(d Display) {
			cmap := d.calibrate()
			out <- d.decode(cmap)
		}(d)
	}
	for i := 0; i < len(displays); i++ {
		res += <-out
	}
	return
}
