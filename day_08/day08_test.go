package day_08

import "testing"

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 26
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 421
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 61229
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 986163
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
