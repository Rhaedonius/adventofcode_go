package day_09

import (
	"os"
	"slices"
	"strings"
)

func parseLine(input string) (res []int) {
	for _, c := range input {
		res = append(res, int(c-'0'))
	}
	return
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (res [][]int) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if s != "" {
			res = append(res, parseLine(s))
		}
	}
	return
}

func calculate_risk(i int, j int, m [][]int) int {
	v := m[i][j]
	rows := len(m) - 1
	cols := len(m[0]) - 1
	if i > 0 && m[i-1][j] <= v {
		return 0
	}
	if j > 0 && m[i][j-1] <= v {
		return 0
	}
	if i < rows && m[i+1][j] <= v {
		return 0
	}
	if j < cols && m[i][j+1] <= v {
		return 0
	}
	return v + 1
}

type Point struct {
	row int
	col int
}

func basin_size(p Point, m [][]int) int {
	// use channel as queue
	ch := make(chan Point, 100)
	rows := len(m) - 1
	cols := len(m[0]) - 1
	var basin []Point
	ch <- p
	for {
		select {
		case p := <-ch:
			v := m[p.row][p.col]
			if v == 9 || slices.Contains(basin, p) {
				continue
			}
			basin = append(basin, p)
			if p.row > 0 && m[p.row-1][p.col] > v {
				ch <- Point{p.row - 1, p.col}
			}
			if p.col > 0 && m[p.row][p.col-1] > v {
				ch <- Point{p.row, p.col - 1}
			}
			if p.row < rows && m[p.row+1][p.col] > v {
				ch <- Point{p.row + 1, p.col}
			}
			if p.col < cols && m[p.row][p.col+1] > v {
				ch <- Point{p.row, p.col + 1}
			}
		default:
			return len(basin)
		}
	}
}

func question1(file string) (res int) {
	heightmap := read_input(file)
	for i, row := range heightmap {
		for j := range row {
			res += calculate_risk(i, j, heightmap)
		}
	}
	return
}

func question2(file string) int {
	heightmap := read_input(file)
	ch := make(chan int)
	to_receive := 0
	for i, row := range heightmap {
		for j := range row {
			if r := calculate_risk(i, j, heightmap); r > 0 {
				to_receive++
				go func(p Point) {
					ch <- basin_size(p, heightmap)
				}(Point{i, j})
			}
		}
	}
	var b1, b2, b3 int
	for i := 0; i < to_receive; i++ {
		b := <-ch
		if b > b3 {
			// b is top3, set to last and fix order
			b3 = b
			if b2 < b3 {
				b2, b3 = b3, b2
			}
			if b1 < b2 {
				b1, b2 = b2, b1
			}
		}
	}
	return b1 * b2 * b3
}
