package day_10

import (
	"os"
	"slices"
	"strings"
)

// reads the file passed as input and retuns an int slice
func ReadInput(file string) (res []string) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if s != "" {
			res = append(res, s)
		}
	}
	return
}

func ErrorScore(s string) (res int, st Stack) {
	for _, c := range s {
		switch c {
		case '(', '[', '{', '<':
			st.push(c)
		case ')':
			if c := st.pop(); c != '(' {
				return 3, st
			}
		case ']':
			if c := st.pop(); c != '[' {
				return 57, st
			}
		case '}':
			if c := st.pop(); c != '{' {
				return 1197, st
			}
		case '>':
			if c := st.pop(); c != '<' {
				return 25137, st
			}
		}
	}
	return 0, st
}

func CompleteScore(st Stack) (res int) {
	for {
		c := st.pop()
		if c == 0 {
			return
		}
		switch c {
		case '(':
			res = res*5 + 1
		case '[':
			res = res*5 + 2
		case '{':
			res = res*5 + 3
		case '<':
			res = res*5 + 4
		}
	}
}

func question1(file string) (res int) {
	input := ReadInput(file)
	for _, s := range input {
		r, _ := ErrorScore(s)
		res += r
	}
	return
}

func question2(file string) int {
	var res []int
	input := ReadInput(file)
	for _, s := range input {
		r, st := ErrorScore(s)
		if r == 0 {
			res = append(res, CompleteScore(st))
		}
	}
	slices.Sort(res)
	return res[len(res)/2]
}
