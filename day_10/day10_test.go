package day_10

import (
	"slices"
	"testing"
)

func TestStack(t *testing.T) {
	var st Stack
	st.push('a')
	st.push('b')
	st.push('c')
	want := []rune{'a', 'b', 'c'}
	if !slices.Equal(st.stack, want) {
		t.Errorf("Wrong push sequence %v. (expected %v)", st.stack, want)
	}
	if c := st.pop(); c != 'c' {
		t.Errorf("Incorrect pop result %c. (expected %x)", c, 'c')
	}
	st.push('d')
	if c := st.pop(); c != 'd' {
		t.Errorf("Incorrect pop result %c. (expected %x)", c, 'd')
	}
	if c := st.pop(); c != 'b' {
		t.Errorf("Incorrect pop result %c. (expected %x)", c, 'b')
	}
}

func TestExample1(t *testing.T) {
	got := question1("example.txt")
	want := 26397
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion1(t *testing.T) {
	got := question1("input.txt")
	want := 389589
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestExample2(t *testing.T) {
	got := question2("example.txt")
	want := 288957
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}

func TestQuestion2(t *testing.T) {
	got := question2("input.txt")
	want := 1190420163
	if got != want {
		t.Errorf("Result %d is not correct. (expected %d)", got, want)
	}
}
