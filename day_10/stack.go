package day_10

type Stack struct {
	stack []rune
}

func (s *Stack) push(r rune) {
	s.stack = append(s.stack, r)
}

func (s *Stack) pop() rune {
	l := len(s.stack)
	if l == 0 {
		return 0
	}
	v := s.stack[l-1]
	s.stack = s.stack[:l-1]
	return v
}
