package day_X

import (
	"os"
	"strconv"
	"strings"
)

func parseLine(input string) int {
	if n, err := strconv.Atoi(input); err != nil {
		return n
	} else {
		panic(err)
	}
}

// reads the file passed as input and retuns an int slice
func read_input(file string) (res []int) {
	input, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	for _, s := range strings.Split(string(input), "\n") {
		if s != "" {
			res = append(res, parseLine(s))
		}
	}
	return
}

func question1(file string) int {
	input := read_input(file)
	return len(input)
}

func question2(file string) int {
	input := read_input(file)
	return len(input)
}
